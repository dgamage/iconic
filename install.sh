#!/bin/bash

#conda create -n tensorflow_env tensorflow                     # create the conda tensorflow virtual environment
#conda activate tensorflow_env

# in the environment install ipykernel 
pip install ipykernel
python -m ipykernel install --user --name=tensorflow

#pip install jupyter
#run jupyter and change the kernel to tensorflow
#jupyter notebook

#packages=($1)

echo "Starting to install packages....." 

   #check virtualenv path 
if [[ "$VIRTUAL_ENV" != "" ]]; then 
    echo "You are in a working virtualenv $VIRTUAL_ENV";
fi

pip install  scikit-learn #
pip install numpy
pip install matplotlib
pip install pandas
pip install  keras
pip install seaborn


