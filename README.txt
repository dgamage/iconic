#############################################################################################################################################
################################### Please read follow the instructions to get the system up and running ####################################
#############################################################################################################################################

1) Some code in the system uses tensorflow. Please install anaconda https://www.anaconda.com/distribution/

* In the terminal or anaconda command prompt type the following to create a virtual environment 

conda create -n tensorflow_env tensorflow                     # create the conda tensorflow virtual environment


* To activate the environemnt type 

conda activate tensorflow_env

 

2) In the activated environment type the following command while in the root directory of this project to setup the environment 
bash install.sh

3) After step2 completes, go to the terminal and type 
jupyter notebook

4) From Jupyter, you can open project notebooks in the "src" folder, and run them. For each notebook change the kernel to tensorflow by seleting from the menu "Kernel->Change Kernel"


********************************************** IMPORTANT NOTE ********************************************************
Please don't run "3_DataLabelling.ipynb" as this will run the data labelling again using K-Means algorithm. K-Means selects the data labels randomly so this may change the final result. If you run this make sure to set the labels properly and you properly understand the coding so you can edit it to get the correct behaviour. 



*********************************************** SQL **********************************************************

5) All SQL script are in the "sql" foler. Uncomment the line and the question number to run the query. The pre-ran results are in sql->result folder. The last question to populate the list of customer you have to run the query as its result is not in the results file. 


 


